from setuptools import find_packages, setup

setup(
    name='imbalanced-data-research',
    packages=find_packages(),
    include_package_data=True,
    version='0',
    description='This project will conduct research on how to handle imbalanced data. ',
    author='daily-research',
    license='',
)
