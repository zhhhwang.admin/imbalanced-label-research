from unittest.mock import patch
from imbalanced_data_research.data import get_data
import pandas as pd


@patch('os.system')
@patch('pandas.read_json')
def test_call_os_system_count(mock_pd_read_json, mock_os_system):

    # Setup mocking
    expected_output = pd.DataFrame({'foo': [1, 2], 'bar': [3, 4]})
    mock_pd_read_json.return_value = expected_output

    # Conduct tests
    test_output = get_data.download_data_from_git()

    # Assertions
    assert mock_os_system.call_count == 4
    assert mock_pd_read_json.call_count == 1
    assert (test_output == expected_output).all
