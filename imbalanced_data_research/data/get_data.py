import os
import pandas as pd
from imbalanced_data_research.config import constants
from imbalanced_data_research.config.logging_config import configure_logger

logger = configure_logger('default', constants.LOG_FILE)


def download_data_from_git():
    """
    Download the zip data from git and return as a pandas dataframe
    :return: pandas data frame
    """

    _ = os.system('curl -LJO ' + constants.GITHUB_DATA_LOCATION)
    _ = os.system('unzip transactions.zip')
    _ = os.system('rm -rf transactions.zip')

    # Read in data and remove txt
    df = pd.read_json("transactions.txt", lines=True)
    _ = os.system('rm -rf transactions.txt')

    # Return the read in file
    return df
